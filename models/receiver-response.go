package models

// ReceiverResponse contains json returned to user to indicate whether operation has been performed successfully.
type ReceiverResponse struct {
	Response string `json:"response"`
}
